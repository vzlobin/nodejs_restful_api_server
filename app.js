const express = require('express');
const app = express();

const productRoutes = require('./api/routes/products');

//Set up COREs for all routes
app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Method', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Header', 'Content-Type');
    next();
});

// Setup filter to target url with /products
app.use('/products', productRoutes);

// Export app to be required;
module.exports = app;