// Require a http package from node.js
const http = require('http');
const app = require("./app.js");
//Either get port form environment variable or use default 3000
const port = process.env.PORT || 3000;

// Create a server
const server = http.createServer(app);

server.listen(port);

