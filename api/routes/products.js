const express = require('express');
const router = express.Router();
// precess only requests sent from app.js targeting /products url
router.get("/", function(req, res, next) {
    res.status(200).json({
        data: [
            {name: "Jack", age: 33, gender: "male"},
            {name: "Jill", age: 35, gender: "female"}
        ]
    });
});
// Post request
router.post("/", function(req, res, next) {
    res.status(200).json({
        message: "Handling POST requests to /products"
    });
});

router.get("/:prodId", function(req, res, next) {
    const id = req.params.prodId;
    var msg = "Prduct with id: " +id+ " is requested";
    if(id === "special") {
        msg = "Special product is found";
    }

    res.status(200).json({
        message: msg
    });
});

router.patch("/:prodId", function(req, res, next) {
    const id = req.params.prodId;
    var msg = "Updated product with id: "+id;
    if(id === "special") {
        msg = "Special product is updated";
    }

    res.status(200).json({
        message: msg
    });
});

router.delete("/:prodId", function(req, res, next) {
    const id = req.params.prodId;
    var msg = "Deleted product with id: "+id;
    if(id === "special") {
        msg = "Special product is Deleted";
    }

    res.status(200).json({
        message: msg
    });
});
module.exports = router;